**# UI Elements Library #**

Library for defining UI elements which can be used in multiple applications. The elements follow the View holder pattern.

To use this library in your application add the following line to the build.gradle in the dependencies module 

**compile 'com.soulpatch.ui:ui:<version> @ aar'** 

Elements added : 
 - TextListItem
 
 - EditListItem