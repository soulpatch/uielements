package soulpatch.com.ui.elements;

import com.soulpatch.ui.elements.BaseListItem;

import org.junit.Test;

/**
 * Tests {@link BaseListItem}
 *
 * @author Akshay Viswanathan
 */
public class BaseListItemTests {

    @Test
    public void constructorTest_null_arguments() {
        new BaseListItem<>(null, null);
        
    }

    @Test
    public void constructorTest_null_data() {
        new BaseListItem<>(null, null);
    }
}
