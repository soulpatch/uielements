package com.soulpatch.ui.elements;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.soulpatch.ui.R;


/**
 * Item with button and its action
 *
 * @author Akshay Viswanathan on 5/30/15.
 */
public class ButtonListItem<T> extends BaseListItem<T> {
    public ButtonListItem(final String title, @NonNull final T data) {
        super(title, data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getViewType() {
        return RowType.BUTTON.ordinal();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewHolder createViewHolder(final LayoutInflater layoutInflater, final ViewGroup parent) {
        final View view = layoutInflater.inflate(R.layout.button_item, parent, false);
        if (view == null) {
            throw new NullPointerException("View could not be inflated");
        }

        if (!(view instanceof TextView)) {
            throw new IllegalArgumentException("view passed in was not a TextView");
        }

        return new ViewHolder(view);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void bindViewHolder(final IListItem.ViewHolder holder) {
        final Button button = ((Button) holder.mRootView);
        button.setText(getTitle());
        super.bindViewHolder(holder);
    }

    public static class ViewHolder extends IListItem.ViewHolder {
        final Button mButtonView;

        public ViewHolder(final View root) {
            super(root);
            mButtonView = (Button) root.findViewById(R.id.buttonView);

            if (mButtonView == null) {
                throw new IllegalArgumentException("Viewholder failed to find all the views");
            }
        }
    }
}
