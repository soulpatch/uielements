package com.soulpatch.ui.elements;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.soulpatch.ui.R;

/**
 * Item to allow the user to edit text
 *
 * @author Akshay Viswanathan
 */
public class CheckBoxItem<T> extends BaseListItem<T> {
    private boolean mChecked;
    private OnCheckBoxStateChangeListener mOnCheckBoxStateChangeListener;

    public interface OnCheckBoxStateChangeListener {
        void onCheckBoxStateChanged(CheckBoxItem checkBoxItem, boolean isChecked);
    }

    public CheckBoxItem(final String title, @NonNull final T data) {
        super(title, data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IListItem.ViewHolder createViewHolder(final LayoutInflater layoutInflater, final ViewGroup parent) {
        final View view = layoutInflater.inflate(R.layout.checkbox_item, parent, false);
        if (view == null) {
            throw new IllegalArgumentException("View inflated is null");
        }

        return new ViewHolder(view);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void bindViewHolder(final IListItem.ViewHolder holder) {
        final CheckBox checkBox = ((CheckBox) holder.mRootView);
        checkBox.setText(getTitle());
        checkBox.setChecked(mChecked);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
                mChecked = isChecked;
                if (mOnCheckBoxStateChangeListener != null) {
                    mOnCheckBoxStateChangeListener.onCheckBoxStateChanged(CheckBoxItem.this, isChecked);
                }
            }
        });
        super.bindViewHolder(holder);
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void setChecked(final boolean checked) {
        mChecked = checked;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getViewType() {
        return RowType.CHECKBOX.ordinal();
    }

    public CheckBoxItem<T> setOnCheckBoxStateChangeListener(final OnCheckBoxStateChangeListener onCheckBoxStateChangeListener) {
        mOnCheckBoxStateChangeListener = onCheckBoxStateChangeListener;
        return this;
    }

    public static class ViewHolder extends IListItem.ViewHolder {
        final CheckBox mCheckBox;

        public ViewHolder(final View root) {
            super(root);
            mCheckBox = (CheckBox) root.findViewById(R.id.checkbox_item);

            if (mCheckBox == null) {
                throw new IllegalArgumentException("Viewholder failed to find all the views");
            }
        }
    }
}
