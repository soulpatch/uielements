package com.soulpatch.ui.elements;

import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.soulpatch.ui.R;
import com.soulpatch.ui.datamodel.BaseData;

/**
 * @author Akshay Viswanathan
 */
public class AddElementItem extends BaseListItem<BaseData> {
	private final String mElementName;

	public AddElementItem(final String elementName) {
		super(elementName);
		mElementName = elementName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IListItem.ViewHolder createViewHolder(final LayoutInflater layoutInflater, final
	ViewGroup parent) {
		final View view = layoutInflater.inflate(R.layout.item_add_element, parent, false);
		if (view == null) {
			throw new IllegalArgumentException("View inflated is null");
		}

		return new ViewHolder(view);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void bindViewHolder(final IListItem.ViewHolder holder) {
		super.bindViewHolder(holder);
		final ViewHolder viewHolder = (ViewHolder) holder;
		viewHolder.addElementLabel.setText(viewHolder.mRootView.getContext().getString(R.string.add_element_label, mElementName));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewType() {
		return IListItem.RowType.TEXT_ITEM.ordinal();
	}

	public static class ViewHolder extends IListItem.ViewHolder {
		final CardView mCardView;
		final TextView addElementLabel;

		public ViewHolder(final View root) {
			super(root);
			addElementLabel = (TextView) root.findViewById(R.id.add_element_label);
			mCardView = (CardView) root.findViewById(R.id.add_element);

			if (mCardView == null || addElementLabel == null) {
				throw new IllegalArgumentException("ViewHolder failed to find all the views");
			}
		}
	}
}
