package com.soulpatch.ui.elements;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.soulpatch.ui.R;

/**
 * Represents item on the Navigation Drawer
 *
 * @author Akshay Viswanathan
 */
public class DrawerListItem<T> extends BaseListItem<T> {
    private boolean mExpanded;
    private int mHierarchy = 1;
    private int mVisibility;

    public DrawerListItem(final String title, final T data) {
        super(title, data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getViewType() {
        return RowType.TEXT_ITEM.ordinal();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewHolder createViewHolder(final LayoutInflater layoutInflater, final ViewGroup parent) {
        final View view = layoutInflater.inflate(R.layout.text_item, parent, false);

        if (!(view instanceof TextView)) {
            throw new IllegalArgumentException("view passed in was not a TextView");
        }

        return new ViewHolder(view);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void bindViewHolder(final IListItem.ViewHolder holder) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        final TextView textView = ((TextView) viewHolder.mRootView);
        textView.setText(getTitle());
        textView.setPadding((int) (viewHolder.mStartPadding * mHierarchy), textView.getPaddingTop(), textView.getPaddingRight(), textView.getPaddingBottom());
        textView.setVisibility(mVisibility);
        super.bindViewHolder(viewHolder);
    }

    public DrawerListItem setHierarchy(final int hierarchy) {
        mHierarchy = hierarchy;
        return this;
    }

    public DrawerListItem toggleExpansion() {
        mExpanded = !mExpanded;
        return this;
    }

    public DrawerListItem expand() {
        mExpanded = true;
        return this;
    }

    public DrawerListItem collapse() {
        mExpanded = false;
        return this;
    }

    public void setVisibility(final int visibility) {
        mVisibility = visibility;
    }

    public boolean isExpanded() {
        return mExpanded;
    }

    public static class ViewHolder extends IListItem.ViewHolder {
        final TextView mTextView;
        final float mStartPadding;

        public ViewHolder(final View root) {
            super(root);
            mTextView = (TextView) root.findViewById(R.id.textview);
            mStartPadding = root.getResources().getDimension(R.dimen.start_padding);

            if (mTextView == null) {
                throw new IllegalArgumentException("Viewholder failed to find all the views");
            }
        }
    }
}
