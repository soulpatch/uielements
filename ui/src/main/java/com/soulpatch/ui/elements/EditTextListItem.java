package com.soulpatch.ui.elements;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.soulpatch.ui.R;

/**
 * Item to allow the user to enter a text
 *
 * @author Akshay Viswanathan
 */
public class EditTextListItem<T> extends BaseListItem<T> {
    public EditTextListItem(final String title, @NonNull final T data) {
        super(title, data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getViewType() {
        return RowType.EDIT_TEXT_ITEM.ordinal();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewHolder createViewHolder(final LayoutInflater layoutInflater, final ViewGroup parent) {
        final View view = layoutInflater.inflate(R.layout.edit_text_item, parent, false);
        if (view == null) {
            throw new NullPointerException("View could not be inflated");
        }

        if (!(view instanceof TextView)) {
            throw new IllegalArgumentException("view passed in was not a TextView");
        }

        return new ViewHolder(view);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void bindViewHolder(final IListItem.ViewHolder holder) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        ((EditText) viewHolder.mRootView).setText(mTitle);
        super.bindViewHolder(viewHolder);
    }

    public static class ViewHolder extends IListItem.ViewHolder {
        final EditText mEditText;

        public ViewHolder(final View root) {
            super(root);
            mEditText = (EditText) root.findViewById(R.id.edit_text);

            if (mEditText == null) {
                throw new IllegalArgumentException("Viewholder failed to find all the views");
            }
        }
    }
}
