package com.soulpatch.ui.elements;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.soulpatch.ui.R;

/**
 * Item that shows the progress bar with a title
 *
 * @author Akshay Viswanathan
 */
public class ProgressBarItem<T> extends BaseListItem<T> {
    private int mProgress;

    public ProgressBarItem(final String title) {
        super(title, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getViewType() {
        return RowType.PROGRESS_BAR_ITEM.ordinal();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewHolder createViewHolder(final LayoutInflater layoutInflater, final ViewGroup parent) {
        final LinearLayout linearLayout = (LinearLayout) layoutInflater.inflate(R.layout.progress_bar_item, parent, false);
        return new ViewHolder(linearLayout);
    }

    public ProgressBarItem setProgress(final int progress) {
        mProgress = progress;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void bindViewHolder(final IListItem.ViewHolder holder) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.mProgressBar.setProgress(mProgress);
        viewHolder.mTitle.setText(mTitle);
        super.bindViewHolder(viewHolder);
    }

    public static class ViewHolder extends IListItem.ViewHolder {
        final ProgressBar mProgressBar;
        final TextView mTitle;

        public ViewHolder(final View root) {
            super(root);
            mProgressBar = (ProgressBar) root.findViewById(R.id.progress_bar_item);
            mTitle = (TextView) root.findViewById(R.id.progress_bar_title);
            if (mProgressBar == null || mTitle == null) {
                throw new IllegalArgumentException("Viewholder failed to find all the views");
            }
        }
    }
}
