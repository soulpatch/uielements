package com.soulpatch.ui.datamodel;

/**
 * Common base class that all the data-models should implement
 *
 * @author Akshay Viswanathan
 */
public class BaseData {
    public String mName;
}